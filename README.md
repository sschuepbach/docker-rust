# docker-rust

Docker images providing the Rust language toolchain including
[cargo](https://github.com/rust-lang/cargo/) and
[clippy](https://github.com/rust-lang/rust-clippy).
